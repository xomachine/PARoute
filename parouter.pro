#-------------------------------------------------
#
# Project created by QtCreator 2018-07-13T00:21:45
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = parouter
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
    PAInterface/peakmanager.cpp \
    gui/rope.cpp \
    gui/stream.cpp \
    main.cpp \
    gui/mainwindow.cpp \
    gui/modulebox.cpp \
    PAInterface/peakmonitor.cpp \
    portinfo.cpp \
    PAInterface/pulseaudio.cpp \
    gui/qvolumecontrol.cpp \
    gui/scenemanager.cpp \
    gui/ssbox.cpp \
    ssinfo.cpp \
    streaminfo.cpp

HEADERS += \
    PAInterface/peakmanager.h \
    gui/mainwindow.h \
    gui/modulebox.h \
    gui/rope.h \
    gui/stream.h \
    PAInterface//peakmonitor.h \
    portinfo.h \
    PAInterface/pulseaudio.h \
    gui/qvolumecontrol.h \
    gui/scenemanager.h \
    gui/ssbox.h \
    ssinfo.h \
    streaminfo.h

FORMS += \
        gui/mainwindow.ui \
        gui/modulebox.ui \
        gui/ssbox.ui \
        gui/stream.ui

unix: CONFIG += link_pkgconfig c++11
unix: PKGCONFIG += libpulse

DISTFILES += \
    .clang-format
