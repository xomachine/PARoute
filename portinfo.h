#ifndef PORTINFO_H
#define PORTINFO_H

#include <QObject>

struct pa_sink_port_info;
struct pa_source_port_info;

struct PortInfo {
  Q_GADGET
public:
  PortInfo() {}
  PortInfo(const pa_sink_port_info *);
  PortInfo(const pa_source_port_info *);
  QString name;
  QString description;
  uint32_t priority;
  int available;
};

#endif // PORTINFO_H
