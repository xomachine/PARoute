#ifndef STREAMINFO_H
#define STREAMINFO_H

#include "portinfo.h"
#include <QMap>
#include <QObject>
#include <QVector>

struct pa_sink_input_info;
struct pa_source_output_info;

struct StreamInfo {
  Q_GADGET
public:
  enum Type { Input, Output, Empty };
  StreamInfo();
  StreamInfo(const pa_sink_input_info *);
  StreamInfo(const pa_source_output_info *);

  QString name;
  QString driver;
  QString serialized_properties;
  QMap<QString, QString> properties;
  QVector<quint32> channel_volumes;
  QVector<double> normalized_volumes;
  QVector<PortInfo> ports;
  uint32_t index;
  uint32_t owner;
  uint32_t client;
  uint32_t source_or_sink;
  uint32_t source = -1;
  Type type;
  bool mute;
  bool corked;
  bool has_volume;
  bool volume_writable;

  const volatile double *peak = nullptr;
};

#endif // STREAMINFO_H
