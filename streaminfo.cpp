#include "streaminfo.h"
#include <pulse/introspect.h>

const static auto mm = qRegisterMetaType<StreamInfo>();
Q_DECLARE_METATYPE(StreamInfo::Type)
const static auto mt = qRegisterMetaType<StreamInfo::Type>();
Q_DECLARE_METATYPE(QVector<quint32>)
const static auto volumesRegistred = qRegisterMetaType<QVector<quint32>>();

StreamInfo::StreamInfo()
  : type(Empty) {}

template <class T> void initSI(StreamInfo &self, const T *info) {
  self.name = info->name;
  self.driver = info->driver;
  self.mute = info->mute;
  self.index = info->index;
  self.owner = info->owner_module;
  self.client = info->client;
  self.corked = info->corked;
  self.has_volume = info->has_volume;
  self.volume_writable = info->volume_writable;
  self.serialized_properties = pa_proplist_to_string(info->proplist);
  for (auto i = 0; i < info->volume.channels; ++i) {
    self.channel_volumes.append(info->volume.values[i]);
    self.normalized_volumes.append(
        double(info->volume.values[i]) /
        double(std::numeric_limits<uint16_t>::max()));
  }
  void *state = nullptr;
  const char *propname = nullptr;
  while ((propname = pa_proplist_iterate(info->proplist, &state))) {
    const char *val = pa_proplist_gets(info->proplist, propname);
    if (val) {
      self.properties.insert(QString(propname), QString(val));
    }
  }
}

StreamInfo::StreamInfo(const pa_sink_input_info *info) {
  source_or_sink = info->sink;
  type = Output;
  initSI(*this, info);
}

StreamInfo::StreamInfo(const pa_source_output_info *info) {
  source_or_sink = info->source;
  source = source_or_sink;
  type = Input;
  initSI(*this, info);
}
