#include "PAInterface/pulseaudio.h"
#include "gui/mainwindow.h"
#include "gui/scenemanager.h"
#include <QApplication>

using namespace std;

int main(int argc, char *argv[]) {
  QApplication a(argc, argv);
  PulseAudio pa;
  MainWindow w;
  SceneManager canvas;
  QThread pulseThread;

  QObject::connect(&pa, SIGNAL(newModule(QString, QString, QString, int)),
                   &canvas, SLOT(addModule(QString, QString, QString, int)));
  QObject::connect(&pa, SIGNAL(clearAll()), &canvas, SLOT(clear()));
  QObject::connect(&pa, SIGNAL(removeModule(int)), &canvas,
                   SLOT(removeModule(int)));
  QObject::connect(&a, SIGNAL(lastWindowClosed()), &pa, SLOT(shutdown()));
  QObject::connect(&a, SIGNAL(lastWindowClosed()), &a, SLOT(quit()));
  QObject::connect(&pa, SIGNAL(connectionStateChanged(QString)), &w,
                   SLOT(logMessage(QString)));
  QObject::connect(&w, SIGNAL(helperModuleVisibilityChanged(bool)), &canvas,
                   SLOT(changeHelperModuleVisibility(bool)));

  w.setScene(canvas.getScene());
  pa.moveToThread(&pulseThread);
  pulseThread.start();
  w.show();
  QMetaObject::invokeMethod(&pa, "init", Qt::QueuedConnection);
  auto retval = a.exec();
  pulseThread.quit();
  pulseThread.wait();
  return retval;
}
