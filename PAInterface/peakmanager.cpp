#include "peakmanager.h"
#include "peakmonitor.h"

PeakManager::PeakManager(pa_context *ctx, QObject *parent)
  : QObject(parent)
  , context(ctx) {}

void PeakManager::attachSSPeak(SSInfo &info) {
  quint32 index =
      info.type == SSInfo::Type::Source ? info.index : info.bound_index;
  PeakMonitor *&monitor = sourceMonitors[index];
  if (!monitor) {
    monitor = new PeakMonitor(context, this);
    monitor->connectSource(index);
  }
  if (info.type == SSInfo::Type::Source) {
    monitorBySink[info.bound_index] = info.index;
    monitor->setNormalizer(1.0 / info.normalized_volumes.first());
  }
  info.peak = monitor->getNormalizedPeak();
}

void PeakManager::attachStreamPeak(StreamInfo &info) {

  if (info.type == StreamInfo::Type::Output) {
    PeakMonitor *&monitor = sinkMonitors[info.index];
    if (!monitor) {
      static double dummy = 0.0;
      auto found = monitorBySink.find(info.source_or_sink);
      if (found == monitorBySink.end()) {
        info.peak = &dummy;
        return;
      }
      monitor = new PeakMonitor(context, this);
      monitor->setNormalizer(1.0 / info.normalized_volumes.first());
      monitor->connectStream(info.index, found.value());
    }
    info.peak = monitor->getNormalizedPeak();

  } else if (info.type == StreamInfo::Type::Input) {
    PeakMonitor *&monitor = sourceMonitors[info.source];
    if (!monitor) {
      monitor = new PeakMonitor(context, this);
      monitor->connectSource(info.source);
    }
    info.peak = monitor->getPeak();
  }
}

void PeakManager::stopSource(quint32 id) {
  auto found = sourceMonitors.find(id);
  if (found != sourceMonitors.end()) {
    sourceMonitors.erase(found);
  }
}

void PeakManager::stopSinkInput(quint32 id) {
  auto found = sinkMonitors.find(id);
  if (found != sinkMonitors.end()) {
    sinkMonitors.erase(found);
  }
}
