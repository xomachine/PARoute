#include "peakmonitor.h"
#include <QtMath>

using namespace std;

PeakMonitor::PeakMonitor(pa_context *ctx, QObject *parent)
  : QObject(parent) {
  pa_sample_spec ss;

  ss.channels = 1;
  ss.format = PA_SAMPLE_FLOAT32;
  ss.rate = 25;

  monitor =
      pa_stream_new(ctx, tr("Peak monitor").toUtf8().constData(), &ss, nullptr);
}

PeakMonitor::~PeakMonitor() {
  if (monitor) {
    disconnect();
    pa_stream_unref(monitor);
    monitor = nullptr;
  }
}

void PeakMonitor::setNormalizer(double coefficient) {
  normalizer = coefficient;
}

const volatile double *PeakMonitor::getPeak() const { return &peak; }

const volatile double *PeakMonitor::getNormalizedPeak() const {
  return &normalizedPeak;
}

bool PeakMonitor::connectSource(quint32 idx) {
  if (!monitor) {
    return false;
  }
  pa_stream_flags_t flags;
  pa_buffer_attr attr;
  char t[16];
  snprintf(t, sizeof(t), "%u", idx);
  memset(&attr, 0, sizeof(attr));
  attr.fragsize = sizeof(float);
  attr.maxlength = (uint32_t)-1;

  pa_stream_set_read_callback(monitor, readCallback, this);
  pa_stream_set_suspended_callback(monitor, suspendCallback, this);

  flags = pa_stream_flags_t(PA_STREAM_DONT_MOVE | PA_STREAM_PEAK_DETECT |
                            PA_STREAM_ADJUST_LATENCY);

  return (pa_stream_connect_record(monitor, t, &attr, flags) == 0);
}

bool PeakMonitor::connectStream(quint32 idx, quint32 source) {

  if (!monitor) {
    return false;
  }
  bool result = (pa_stream_set_monitor_stream(monitor, idx) >= 0);
  return result && connectSource(source);
}

void PeakMonitor::disconnect() {
  if (!monitor) {
    return;
  }
  pa_stream_disconnect(monitor);
}

void PeakMonitor::readCallback(pa_stream *s, size_t length, void *userdata) {
  auto self = reinterpret_cast<PeakMonitor *>(userdata);
  const float *data;
  pa_stream_peek(s, reinterpret_cast<const void **>(&data), &length);
  if (!data) {
    if (length > 0) {
      pa_stream_drop(s);
    }
    return;
  }
  double value = data[length / sizeof(float) - 1];
  pa_stream_drop(s);
  if (!self->monitor) {
    return;
  }
  value = min(max(0.0, value), 1.0);
  self->peak = value;
  self->normalizedPeak = value * self->normalizer;
}

void PeakMonitor::suspendCallback(pa_stream *, void *userdata) {
  auto self = reinterpret_cast<PeakMonitor *>(userdata);
  self->peak = 0.0;
  self->normalizedPeak = 0.0;
}
