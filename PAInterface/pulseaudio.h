#ifndef PULSEAUDIO_H
#define PULSEAUDIO_H

#include "peakmonitor.h"
#include "ssinfo.h"
#include "streaminfo.h"
#include <QtCore>
#include <pulse/introspect.h>
#include <pulse/mainloop.h>

Q_DECLARE_LOGGING_CATEGORY(pulse_audio)

class PeakManager;

class PulseAudio : public QObject {
  Q_OBJECT
public:
  PulseAudio();
  ~PulseAudio();
public slots:
  void init();
  void shutdown();
  void refreshSSInfo(int module = -1, int srcidx = -1);
  void refreshSinkInfo(int idx = -1);
  void refreshInStreamInfo(int module = -1, int idx = -1);
  void refreshOutStreamInfo(int module = -1, int idx = -1);
  void changeStreamDestination(quint32 stream, quint32 destination,
                               bool is_input);
  void changeStreamVolume(quint32 stream, QVector<quint32> value,
                          bool is_input);
  void muteStream(quint32 stream, bool mute, bool is_input);
  void changeSSVolume(quint32 index, QVector<quint32> values, bool is_sink);
  void muteSS(quint32 index, bool mute, bool is_sink);
  void setDefaultSS(QString name, bool is_sink);
signals:
  void connected();
  void disconnected();
  void connectionStateChanged(QString);
  void failedToConnect();
  void ready();
  void newModule(QString name, QString argument, QString properties, int idx);
  void removeModule(int);
  void clearAll();
  void newSS(SSInfo source_or_sink);
  void removeSS(int, SSInfo::Type);
  void newStream(StreamInfo);
  void removeStream(quint32, StreamInfo::Type);
  void initalizationComplete();
  void defaultSSChanged(QString name, QString sink_name);

  void pulseAudioEvent(quint32 subject, quint32 action, quint32 index);
private slots:
  void iterate();
  void subscribe();
  void refreshModuleInfo(int idx = -1);
  void refreshServerInfo();

private:
  enum class InitStep {
    Created,
    Connected,
    ModulesEnumerated,
    SourcesEnumerated,
    SinksEnumerated,
    HalfStreamsEnumerated,
    Initialized
  };

  static void onNotify(pa_context *ctx, pa_subscription_event_type_t eventType,
                       uint32_t idx, void *userdata);
  static void onState(pa_context *ctx, void *userdata);
  static void moduleInfoCb(pa_context *, const pa_module_info *info, int eol,
                           void *userdata);
  static void srcInfoCb(pa_context *ctx, const pa_source_info *info, int eol,
                        void *userdata);
  static void sinkInfoCb(pa_context *, const pa_sink_info *info, int eol,
                         void *userdata);

  template <class T>
  friend void streamInfoCb(pa_context *, const T *info, int eol,
                           void *userdata);

  pa_mainloop *loop = nullptr;
  pa_mainloop_api *loop_api = nullptr;
  pa_context *ctx = nullptr;
  QTimer *puller = nullptr;
  QLoggingCategory logger;
  InitStep initializationStep = InitStep::Created;
  PeakManager *peakManager = nullptr;
};

#endif // PULSEAUDIO_H
