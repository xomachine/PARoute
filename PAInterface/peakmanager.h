#ifndef PEAKMANAGER_H
#define PEAKMANAGER_H
#include "ssinfo.h"
#include "streaminfo.h"
#include <QMap>
#include <QObject>

struct pa_context;
class PeakMonitor;

/**
 * @brief The PeakManager class hides all specific pulse audio peak
 * reading quirks and gives clear interface for all sinks, sources
 * and stream where each entity has its own pointer to the peak
 * value, so it does need to know what some of the values
 * are actually the same or calculated from other or whatever
 */
class PeakManager : public QObject {
  Q_OBJECT
public:
  PeakManager(pa_context *ctx, QObject *parent);

  void attachSSPeak(SSInfo &);
  void attachStreamPeak(StreamInfo &);

  void stopSource(quint32);
  void stopSink(quint32) {}
  void stopSourceOutput(quint32) {}
  void stopSinkInput(quint32);

private:
  pa_context *context;

  QMap<quint32, QPair<volatile double, double>> sourceOutputs;
  QMap<quint32, QPair<volatile double, double>> sinkInputs;
  QMap<quint32, QPair<volatile double, double>> sources;
  QMap<quint32, QPair<volatile double, double>> sinks;

  QMap<quint32, PeakMonitor *> sourceMonitors;
  QMap<quint32, PeakMonitor *> sinkMonitors;
  QMap<quint32, quint32> monitorBySink;
};

#endif // PEAKMANAGER_H
