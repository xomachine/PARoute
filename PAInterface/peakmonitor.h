#ifndef PEAKMONITOR_H
#define PEAKMONITOR_H
#include <QObject>
#include <functional>
#include <pulse/context.h>
#include <pulse/stream.h>

class PeakMonitor : public QObject {
  Q_OBJECT
public:
  PeakMonitor(pa_context *ctx, QObject *parent = nullptr);
  ~PeakMonitor();

  void setNormalizer(double coefficient);
  const volatile double *getPeak() const;
  const volatile double *getNormalizedPeak() const;

public slots:
  bool connectSource(quint32 idx);
  bool connectStream(quint32 idx, quint32 source);
  void disconnect();

private:
  static void readCallback(pa_stream *, size_t, void *);
  static void suspendCallback(pa_stream *, void *);

  volatile double peak;
  volatile double normalizedPeak;
  pa_stream *monitor = nullptr;
  double normalizer = 1.0;
};

#endif // PEAKMONITOR_H
