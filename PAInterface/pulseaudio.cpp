#include "pulseaudio.h"
#include <pulse/subscribe.h>
#include <pulse/xmalloc.h>
#include <utility>

#include "peakmanager.h"

Q_LOGGING_CATEGORY(pulse_audio, "pulseaudio")

using namespace std;

const auto APPLICATION_NAME = "PARouter";

struct filterUD {
  PulseAudio *self;
  int modfilter;
};

void PulseAudio::onNotify(pa_context *ctx,
                          pa_subscription_event_type_t eventType, uint32_t idx,
                          void *userdata) {
  auto self = reinterpret_cast<PulseAudio *>(userdata);
  auto facility = eventType & PA_SUBSCRIPTION_EVENT_FACILITY_MASK;
  auto type = eventType & PA_SUBSCRIPTION_EVENT_TYPE_MASK;
  emit self->connectionStateChanged(
      tr(("Got notify " + to_string(eventType) + " idx = " + to_string(idx))
             .c_str()));
  qCDebug(self->logger, "%p: Got notify t:%x,f:%x, idx=%d", ctx, type, facility,
          idx);
  emit self->pulseAudioEvent(facility, type, idx);
  switch (facility) {
  case PA_SUBSCRIPTION_EVENT_SERVER:
    switch (type) {
    case PA_SUBSCRIPTION_EVENT_CHANGE:
      qCDebug(self->logger, "Server change event occured");
      emit self->refreshServerInfo();
      break;
    }
    break;
  case PA_SUBSCRIPTION_EVENT_MODULE:
    switch (type) {
    case PA_SUBSCRIPTION_EVENT_REMOVE:
      emit self->removeModule(idx);
      qCDebug(self->logger, "Module remove %d", idx);
      break;
    default:
      qCDebug(self->logger, "Module refresh %d", idx);
      self->refreshModuleInfo(idx);
      break;
    }
    break;
  case PA_SUBSCRIPTION_EVENT_SOURCE:
    switch (type) {
    case PA_SUBSCRIPTION_EVENT_REMOVE:
      emit self->removeSS(idx, SSInfo::Type::Source);
      qCDebug(self->logger, "Source remove %d", idx);
      self->peakManager->stopSource(idx);
      break;
    default:
      qCDebug(self->logger, "Source refresh %d", idx);
      self->refreshSSInfo(-1, idx);
      break;
    }
    break;
  case PA_SUBSCRIPTION_EVENT_SINK:
    switch (type) {
    case PA_SUBSCRIPTION_EVENT_REMOVE:
      emit self->removeSS(idx, SSInfo::Type::Sink);
      self->peakManager->stopSink(idx);
      qCDebug(self->logger, "Sink remove %d", idx);
      break;
    default:
      qCDebug(self->logger, "Sink refresh %d", idx);
      self->refreshSinkInfo(idx);
      break;
    }
    break;
  case PA_SUBSCRIPTION_EVENT_SOURCE_OUTPUT:
    switch (type) {
    case PA_SUBSCRIPTION_EVENT_REMOVE:
      qCDebug(self->logger, "Source output remove %d", idx);
      emit self->removeStream(idx, StreamInfo::Input);
      self->peakManager->stopSourceOutput(idx);
      break;
    default:
      qCDebug(self->logger, "Source output refresh %d", idx);
      self->refreshInStreamInfo(-1, idx);
      break;
    }
    break;
  case PA_SUBSCRIPTION_EVENT_SINK_INPUT:
    switch (type) {
    case PA_SUBSCRIPTION_EVENT_REMOVE:
      qCDebug(self->logger, "Sink input remove %d", idx);
      emit self->removeStream(idx, StreamInfo::Output);
      self->peakManager->stopSinkInput(idx);
      break;
    default:
      qCDebug(self->logger, "Sink input refresh %d", idx);
      self->refreshOutStreamInfo(-1, idx);
      break;
    }
    break;
  default:
    break;
  }
}

void PulseAudio::onState(pa_context *ctx, void *userdata) {
  auto self = reinterpret_cast<PulseAudio *>(userdata);
  auto state = pa_context_get_state(ctx);
  qCDebug(self->logger, "Got state change: %d", state);
  switch (state) {
  case PA_CONTEXT_READY:
    emit self->connected();
    emit self->connectionStateChanged(tr("Connected!"));
    break;
  case PA_CONTEXT_FAILED:
    emit self->failedToConnect();
    emit self->connectionStateChanged(tr("Failed to connect!"));
    break;
  case PA_CONTEXT_TERMINATED:
    emit self->disconnected();
    emit self->connectionStateChanged(tr("Connection closed"));
    break;
  case PA_CONTEXT_CONNECTING:
    emit self->connectionStateChanged(tr("Connecting..."));
    break;
  case PA_CONTEXT_AUTHORIZING:
    emit self->connectionStateChanged(tr("Authorization..."));
    break;
  case PA_CONTEXT_SETTING_NAME:
    emit self->connectionStateChanged(tr("Setting application name..."));
    break;
  default:
    emit self->connectionStateChanged(tr("Unknown state reported!"));
    break;
  }
}

PulseAudio::PulseAudio()
  : logger("pulseaudio") {
  logger.setEnabled(QtMsgType::QtDebugMsg, true);
  loop = pa_mainloop_new();
}

PulseAudio::~PulseAudio() {
  shutdown();
  pa_mainloop_free(loop);
}

void PulseAudio::iterate() {
  int retval = 0;
  int status = pa_mainloop_iterate(loop, 0, &retval);
  if (status < 0) {
    emit failedToConnect();
    emit connectionStateChanged("Error ocured while performing iteration");
    shutdown();
  }
}

void PulseAudio::subscribe() {
  pa_context_set_subscribe_callback(ctx, onNotify, this);
  auto subsFuture = pa_context_subscribe(
      ctx, PA_SUBSCRIPTION_MASK_ALL,
      [](pa_context *, int, void *ud) {
        auto self = reinterpret_cast<PulseAudio *>(ud);
        emit self->connectionStateChanged("Subscribtion done");
        emit self->ready();
      },
      this);
  pa_operation_unref(subsFuture);
}

void PulseAudio::moduleInfoCb(pa_context *, const pa_module_info *info, int eol,
                              void *userdata) {
  auto self = reinterpret_cast<PulseAudio *>(userdata);
  if (eol) {
    if (self->initializationStep < InitStep::ModulesEnumerated) {
      self->initializationStep = InitStep::ModulesEnumerated;
      emit self->refreshSSInfo();
    }
    return;
  }
  auto charprops = pa_proplist_to_string(info->proplist);
  QString properties(charprops);
  pa_xfree(charprops);
  emit self->newModule(QString(info->name), QString(info->argument), properties,
                       info->index);
  emit self->connectionStateChanged(
      QString("Found module %1: %2").arg(info->index).arg(info->name));
}

void PulseAudio::refreshModuleInfo(int idx) {
  if (idx < 0) {
    emit clearAll();
    pa_context_get_module_info_list(ctx, moduleInfoCb, this);
  } else {
    pa_context_get_module_info(ctx, idx, moduleInfoCb, this);
  }
}

void PulseAudio::refreshServerInfo() {
  pa_server_info_cb_t cb = [](pa_context *, const pa_server_info *info,
                              void *selfPtr) {
    auto self = reinterpret_cast<PulseAudio *>(selfPtr);
    QString name(info->default_source_name);
    QString sink_name(info->default_sink_name);
    emit self->defaultSSChanged(name, sink_name);
  };
  pa_context_get_server_info(ctx, cb, this);
}

static void emptyCallback(pa_context *, int, void *) {}

void PulseAudio::changeStreamDestination(quint32 stream, quint32 destination,
                                         bool is_input) {
  if (is_input) {
    pa_context_move_source_output_by_index(ctx, stream, destination,
                                           emptyCallback, this);
  } else {
    peakManager->stopSinkInput(stream);
    pa_context_move_sink_input_by_index(ctx, stream, destination, emptyCallback,
                                        this);
  }
}

static pa_cvolume toPaVolume(const QVector<quint32> &values) {
  pa_cvolume vol;
  vol.channels = values.size();
  size_t i = 0;
  for (auto value : values) {
    vol.values[i++] = value;
  }
  return vol;
}

void PulseAudio::changeStreamVolume(quint32 stream, QVector<quint32> values,
                                    bool is_input) {
  auto vol = toPaVolume(values);
  if (is_input) {
    pa_context_set_source_output_volume(ctx, stream, &vol, emptyCallback, this);
  } else {
    pa_context_set_sink_input_volume(ctx, stream, &vol, emptyCallback, this);
  }
}

void PulseAudio::muteStream(quint32 stream, bool mute, bool is_input) {
  if (is_input) {
    pa_context_set_source_output_mute(ctx, stream, mute, emptyCallback, this);
  } else {
    pa_context_set_sink_input_mute(ctx, stream, mute, emptyCallback, this);
  }
}

void PulseAudio::changeSSVolume(quint32 index, QVector<quint32> values,
                                bool is_sink) {
  auto volumes = toPaVolume(values);
  if (is_sink) {
    pa_context_set_sink_volume_by_index(ctx, index, &volumes, emptyCallback,
                                        this);
  } else {
    pa_context_set_source_volume_by_index(ctx, index, &volumes, emptyCallback,
                                          this);
  }
}

void PulseAudio::muteSS(quint32 index, bool mute, bool is_sink) {
  if (is_sink) {
    pa_context_set_sink_mute_by_index(ctx, index, mute, emptyCallback, this);
  } else {
    pa_context_set_source_mute_by_index(ctx, index, mute, emptyCallback, this);
  }
}

void PulseAudio::setDefaultSS(QString name, bool is_sink) {
  if (is_sink) {
    pa_context_set_default_sink(ctx, name.toUtf8().data(), emptyCallback, this);
  } else {
    pa_context_set_default_source(ctx, name.toUtf8().data(), emptyCallback,
                                  this);
  }
}

void PulseAudio::sinkInfoCb(pa_context *, const pa_sink_info *info, int eol,
                            void *userdata) {
  auto self = reinterpret_cast<PulseAudio *>(userdata);
  if (eol) {
    if (self->initializationStep < InitStep::SinksEnumerated) {
      self->initializationStep = InitStep::SinksEnumerated;
      emit self->refreshInStreamInfo();
      emit self->refreshOutStreamInfo();
      emit self->refreshServerInfo();
    }
    return;
  }

  SSInfo result(info);
  self->peakManager->attachSSPeak(result);

  emit self->connectionStateChanged("Found sink " + result.name);
  emit self->newSS(result);
}

template <class T>
void streamInfoCb(pa_context *, const T *info, int eol, void *userdata) {
  auto filter = reinterpret_cast<filterUD *>(userdata);
  if (eol) {
    if (filter->self->initializationStep <
        PulseAudio::InitStep::HalfStreamsEnumerated) {
      filter->self->initializationStep =
          PulseAudio::InitStep::HalfStreamsEnumerated;
    } else if (filter->self->initializationStep <
               PulseAudio::InitStep::Initialized) {
      filter->self->initializationStep = PulseAudio::InitStep::Initialized;
      emit filter->self->initalizationComplete();
    }
    delete filter;
    return;
  }
  if (filter->modfilter >= 0 &&
      uint32_t(filter->modfilter) != info->owner_module) {
    return;
  }
  if (info->resample_method && strcmp(info->resample_method, "peaks") == 0) {
    return;
  }
  StreamInfo stream(info);
  filter->self->peakManager->attachStreamPeak(stream);
  emit filter->self->newStream(stream);
}

void PulseAudio::refreshSinkInfo(int idx) {
  if (idx < 0) {
    pa_context_get_sink_info_list(ctx, sinkInfoCb, this);
  } else {
    pa_context_get_sink_info_by_index(ctx, idx, sinkInfoCb, this);
  }
}

void PulseAudio::refreshInStreamInfo(int module, int idx) {
  auto filter = new filterUD{this, module};
  if (idx < 0) {
    pa_context_get_source_output_info_list(
        ctx, streamInfoCb<pa_source_output_info>, filter);
  } else {
    pa_context_get_source_output_info(
        ctx, uint32_t(idx), streamInfoCb<pa_source_output_info>, filter);
  }
}

void PulseAudio::refreshOutStreamInfo(int module, int idx) {
  auto filter = new filterUD{this, module};
  if (idx < 0) {
    pa_context_get_sink_input_info_list(ctx, streamInfoCb<pa_sink_input_info>,
                                        filter);
  } else {
    pa_context_get_sink_input_info(ctx, idx, streamInfoCb<pa_sink_input_info>,
                                   filter);
  }
}

void PulseAudio::srcInfoCb(pa_context *, const pa_source_info *info, int eol,
                           void *userdata) {
  auto filter = reinterpret_cast<filterUD *>(userdata);
  if (eol) {
    if (filter->self->initializationStep < InitStep::SourcesEnumerated) {
      filter->self->initializationStep = InitStep::SourcesEnumerated;
      emit filter->self->refreshSinkInfo();
    }
    delete filter;
    return;
  }
  if (filter->modfilter >= 0 &&
      info->owner_module != uint32_t(filter->modfilter)) {
    return;
  }

  SSInfo result(info);

  if (info->monitor_of_sink != PA_INVALID_INDEX &&
      filter->self->initializationStep > InitStep::SourcesEnumerated) {
    auto newfilter = new filterUD(*filter);
    emit filter->self->connectionStateChanged("Require mon for source " +
                                              result.name);
    newfilter->self->refreshSinkInfo(info->monitor_of_sink);
  }

  filter->self->peakManager->attachSSPeak(result);
  emit filter->self->connectionStateChanged("Found source " + result.name);
  emit filter->self->newSS(result);
}

void PulseAudio::refreshSSInfo(int module, int srcidx) {
  auto filter = new filterUD{this, module};
  if (srcidx < 0) {
    pa_context_get_source_info_list(ctx, srcInfoCb, filter);
  } else {
    pa_context_get_source_info_by_index(ctx, srcidx, srcInfoCb, filter);
  }
}

void PulseAudio::init() {
  if (ctx) {
    return;
  }
  loop_api = pa_mainloop_get_api(loop);
  ctx = pa_context_new(loop_api, APPLICATION_NAME);
  pa_context_set_state_callback(ctx, onState, this);
  peakManager = new PeakManager(ctx, this);
  puller = new QTimer(this);
  connect(puller, SIGNAL(timeout()), this, SLOT(iterate()));
  connect(this, SIGNAL(connected()), this, SLOT(subscribe()));
  connect(this, SIGNAL(ready()), this, SLOT(refreshModuleInfo()));
  puller->start(50);
  if (pa_context_connect(ctx, nullptr, PA_CONTEXT_NOFAIL, nullptr) < 0) {
    emit failedToConnect();
  }
}

void PulseAudio::shutdown() {
  if (ctx) {
    pa_context_disconnect(ctx);
    pa_context_unref(ctx);
    ctx = nullptr;
  }
  puller->stop();
}
