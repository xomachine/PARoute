#ifndef SINKINFO_H
#define SINKINFO_H

#include "portinfo.h"
#include <QMap>
#include <QObject>
#include <QVector>

struct pa_sink_info;
struct pa_source_info;

struct SSInfo {
  Q_GADGET
public:
  enum class Type { Unknown, Source, Sink };

  SSInfo();
  SSInfo(const pa_sink_info *);
  SSInfo(const pa_source_info *);

  QString name;
  uint32_t index;
  uint32_t bound_index;
  QString description;
  uint32_t owner;
  bool mute;
  QString driver;
  QMap<QString, QString> properties;
  QString serialized_properties;
  uint32_t card;
  uint32_t volume_steps;
  uint32_t base_volume;
  QVector<uint32_t> channel_volumes;
  QVector<double> normalized_volumes;
  QVector<PortInfo> ports;
  PortInfo active_port;
  Type type = Type::Unknown;

  const volatile double *peak = nullptr;

  bool isEmpty();

private:
  bool initialized = true;
};

#endif // SINKINFO_H
