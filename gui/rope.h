#ifndef ROPE_H
#define ROPE_H

#include <QGraphicsPathItem>
#include <QGraphicsWidget>

class Rope : public QObject, public QGraphicsPathItem {
  Q_OBJECT
public:
  explicit Rope(qreal shift = 100.0, QGraphicsItem *parent = nullptr);

signals:

public slots:
  void startMoved(QPointF);
  void endMoved(QPointF);

private:
  virtual void hoverEnterEvent(QGraphicsSceneHoverEvent *event) override;
  virtual void hoverLeaveEvent(QGraphicsSceneHoverEvent *event) override;
  void repaint();

  QPointF start;
  QPointF end;
  qreal shift;
  bool selected = false;
};

#endif // ROPE_H
