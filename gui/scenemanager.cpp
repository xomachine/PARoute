#include "scenemanager.h"
#include "modulebox.h"
#include "ssbox.h"
#include <QGraphicsProxyWidget>

SceneManager::SceneManager(QObject *parent)
  : QObject(parent)
  , scene(this)
  , layout(new QGraphicsLinearLayout(Qt::Orientation::Vertical))
  , holder(new QGraphicsWidget) {
  holder->setLayout(layout);
  scene.addItem(holder);
}

QGraphicsScene *SceneManager::getScene() { return &scene; }

void SceneManager::addModule(QString name, QString argument, QString props,
                             int idx) {
  auto exist = modules.find(idx);
  if (exist != modules.end()) {
    emit reinitModule(name, argument, props, idx);
    return;
  }
  auto newModule = new ModuleBox(name, argument, props, idx, holder);
  connect(this, SIGNAL(reinitModule(QString, QString, QString, uint32_t)),
          newModule, SLOT(reinit(QString, QString, QString, uint32_t)));
  connect(newModule, SIGNAL(streamOriginConnected(QWidget *, StreamInfo)), this,
          SIGNAL(attachStream(QWidget *, StreamInfo)));
  connect(sender(), SIGNAL(newSS(SSInfo)), newModule, SLOT(addSS(SSInfo)));
  connect(sender(), SIGNAL(removeSS(int, SSInfo::Type)), newModule,
          SLOT(removeSS(int, SSInfo::Type)));
  connect(sender(), SIGNAL(newStream(StreamInfo)), newModule,
          SLOT(addStream(StreamInfo)));
  connect(sender(), SIGNAL(removeStream(quint32, StreamInfo::Type)), newModule,
          SLOT(removeStream(quint32, StreamInfo::Type)));
  connect(this, SIGNAL(helperModuleVisibilityChanged(bool)), newModule,
          SLOT(changeHelperModuleVisibility(bool)));

  auto proxy = newModule->graphicsProxyWidget();

  layout->addItem(proxy);
  layout->setAlignment(proxy, {Qt::AlignmentFlag::AlignHCenter});

  modules.insert(idx, newModule);
}

void SceneManager::removeModule(int idx) {
  auto module = modules.find(idx);
  if (module == modules.end()) {
    return;
  }
  QMetaObject::invokeMethod(module.value(), "deleteLater");
  modules.erase(module);
}

void SceneManager::clear() {
  for (auto module : modules) {
    QMetaObject::invokeMethod(module, "deleteLater");
  }
  modules.clear();
}

void SceneManager::changeHelperModuleVisibility(bool visible) {
  showHelperModules = visible;
  emit helperModuleVisibilityChanged(visible);
}
