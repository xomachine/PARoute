#include "ssbox.h"
#include "pulse/def.h"
#include "ui_ssbox.h"
#include <QGraphicsProxyWidget>
#include <QStyle>
#include <QTimer>
#include <limits>

constexpr double maximum = double(std::numeric_limits<uint16_t>::max());

SSBox::SSBox(QWidget *parent)
  : QFrame(parent)
  , ui(new Ui::SSBox) {
  ui->setupUi(this);
  ui->sinkVolCtl->setRange(0, maximum);
  ui->sourceVolCtl->setRange(0, maximum);
  setAcceptDrops(true);
  ui->sinkStreamPlaceholder->hide();
  ui->sourceStreamPlaceholder->hide();
  ui->sinkMute->setIcon(style()->standardIcon(QStyle::SP_MediaVolumeMuted));
  ui->sourceMute->setIcon(style()->standardIcon(QStyle::SP_MediaVolumeMuted));
  ui->defaultSink->setIcon(style()->standardIcon(QStyle::SP_DialogApplyButton));
  ui->defaultSource->setIcon(
      style()->standardIcon(QStyle::SP_DialogApplyButton));
  connect(ui->sinkMute, SIGNAL(toggled(bool)), this, SLOT(muteSink(bool)));
  connect(ui->sourceMute, SIGNAL(toggled(bool)), this, SLOT(muteSource(bool)));
  connect(ui->defaultSink, SIGNAL(released()), this, SLOT(setDefaultSink()));
  connect(ui->defaultSource, SIGNAL(released()), this,
          SLOT(setDefaultSource()));
  connect(ui->sourceVolCtl, SIGNAL(sliderMoved(int)), this,
          SLOT(changeVolumeSource(int)));
  connect(ui->sinkVolCtl, SIGNAL(sliderMoved(int)), this,
          SLOT(changeVolumeSink(int)));
  connect(ui->sourceVolCtl, SIGNAL(sliderPressed()), this,
          SLOT(startIgnoringUpdates()));
  connect(ui->sinkVolCtl, SIGNAL(sliderPressed()), this,
          SLOT(startIgnoringUpdates()));
  connect(ui->sourceVolCtl, SIGNAL(sliderReleased()), this,
          SLOT(stopIgnoringUpdates()));
  connect(ui->sinkVolCtl, SIGNAL(sliderReleased()), this,
          SLOT(stopIgnoringUpdates()));
  connect(&peaker, SIGNAL(timeout()), this, SLOT(readPeak()));
  peaker.start(50);
}

SSBox::SSBox(uint32_t idx, QWidget *parent)
  : SSBox(parent) {
  index = idx;
}

SSBox::~SSBox() { delete ui; }

void SSBox::reinit(SSInfo source_or_sink) {
  if (ignoreUpdates) {
    return;
  }
  size_t volume = source_or_sink.channel_volumes[0];
  switch (source_or_sink.type) {
  case SSInfo::Type::Source: {
    if (source_or_sink.bound_index == PA_INVALID_INDEX) {
      ui->sinkHalf->setDisabled(true);
      ui->sinkBox->setTitle("");
    }
    sourcePeak = source_or_sink.peak;
    index = source_or_sink.index;
    ui->sourceMute->setDown(source_or_sink.mute);
    ui->sourceBox->setTitle(source_or_sink.description);
    ui->sourceBox->setToolTip(source_or_sink.serialized_properties);
    channels = source_or_sink.channel_volumes.size();
    name = source_or_sink.name;
    ui->sourceVolCtl->setValue(volume);
  } break;
  case SSInfo::Type::Sink: {
    index = source_or_sink.bound_index;
    sinkPeak = source_or_sink.peak;
    ui->sinkHalf->setDisabled(false);
    ui->sinkMute->setDown(source_or_sink.mute);
    ui->sinkBox->setTitle(source_or_sink.description);
    ui->sinkBox->setToolTip(source_or_sink.serialized_properties);
    sink_index = source_or_sink.index;
    sink_channels = source_or_sink.channel_volumes.size();
    sink_name = source_or_sink.name;
    ui->sinkVolCtl->setValue(volume);

  } break;
  default:
    return;
  }
}

void SSBox::setDefaultSS(QString src_name, QString snk_name) {
  ui->defaultSource->setChecked(name == src_name);
  ui->defaultSink->setChecked(snk_name == sink_name);
}

void SSBox::readPeak() {
  if (sourcePeak) {
    ui->sourceVolCtl->setProgress(*sourcePeak *
                                  double(ui->sourceVolCtl->value() / maximum));
  }
  if (sinkPeak && ui->sinkVolCtl->isEnabled()) {
    ui->sinkVolCtl->setProgress(*sinkPeak *
                                double(ui->sinkVolCtl->value() / maximum));
  }
}

constexpr static auto afterPlaceholderPosition = 1;

void SSBox::attachStream(QWidget *streamPlug, StreamInfo info) {
  quint32 comparator = 0;
  QVBoxLayout *container = nullptr;
  switch (info.type) {
  case StreamInfo::Type::Input:
    comparator = index;
    container = ui->sourceStreamContainer;
    disconnect(streamPlug, SLOT(changeNormalizedPeak(qreal)));
    connect(this, SIGNAL(normalizedPeakChanged(qreal)), streamPlug,
            SLOT(changeNormalizedPeak(qreal)));
    break;
  case StreamInfo::Type::Output:
    comparator = sink_index;
    container = ui->sinkStreamContainer;
    break;
  default:
    return;
  }
  if (comparator == info.source_or_sink) {
    container->insertWidget(afterPlaceholderPosition, streamPlug);
    graphicsProxyWidget()->createProxyForChildWidget(streamPlug);
  }
}

void SSBox::dropHover(QSize *dropSize, bool is_input) {
  ui->sinkStreamPlaceholder->hide();
  ui->sourceStreamPlaceholder->hide();

  if (dropSize) {
    auto target = ui->sinkStreamPlaceholder;
    if (is_input) {
      target = ui->sourceStreamPlaceholder;
    }
    QPixmap map(target->parentWidget()->width(), dropSize->height());
    map.fill(Qt::green);
    target->setPixmap(map);
    target->show();
  }
}

void SSBox::changeVolumeSource(int value) {
  QVector<quint32> volumes(channels);
  for (auto &volume : volumes) {
    volume = value;
  }
  emit volumeChanged(index, volumes, false);
}

void SSBox::muteSource(bool is_muted) { emit muted(index, is_muted, false); }

void SSBox::changeVolumeSink(int value) {
  QVector<quint32> volumes(channels);
  for (auto &volume : volumes) {
    volume = value;
  }
  emit volumeChanged(sink_index, volumes, true);
}

void SSBox::muteSink(bool is_muted) { emit muted(sink_index, is_muted, true); }

void SSBox::setDefaultSink() {
  QMetaObject::invokeMethod(sender(), "setChecked", Q_ARG(bool, false));
  emit defaultSSSet(sink_name, true);
}

void SSBox::setDefaultSource() {
  QMetaObject::invokeMethod(sender(), "setChecked", Q_ARG(bool, false));
  emit defaultSSSet(name, false);
}
