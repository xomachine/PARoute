#include "qvolumecontrol.h"
#include <QPaintEvent>
#include <QPainter>
#include <QStyleOption>
#include <QStylePainter>
#include <QTimer>

QVolumeControl::QVolumeControl(QWidget *parent)
  : QSlider(parent)
  , peakFadeTimer(new QTimer(this)) {
  connect(peakFadeTimer, SIGNAL(timeout()), this, SLOT(setProgress()));
  peakFadeTimer->start(200);
}

void QVolumeControl::paintEvent(QPaintEvent *) {
  QPainter p(this);
  QStyleOptionSlider opt;
  initStyleOption(&opt);
  constexpr auto maxProgressTicks = 1000;
  constexpr auto ticksInterval = 8192;
  constexpr auto peakThickness = 10;
  opt.pageStep = ticksInterval;

  QStyleOptionProgressBar pgOpt;
  pgOpt.initFrom(this);
  pgOpt.orientation = orientation();
  pgOpt.minimum = 0;
  pgOpt.maximum = maxProgressTicks;
  pgOpt.progress = lastValue * maxProgressTicks;
  pgOpt.invertedAppearance = invertedAppearance();
  auto newrect = rect();
  if (pgOpt.orientation == Qt::Orientation::Vertical) {
    newrect.setX(newrect.x() + width() / 2 - peakThickness / 2);
    newrect.setWidth(peakThickness);
  } else {
    newrect.setY(newrect.y() + height() / 2 - peakThickness / 2);
    newrect.setHeight(peakThickness);
  }
  pgOpt.rect = newrect;
  style()->drawControl(QStyle::CE_ProgressBar, &pgOpt, &p, this);

  opt.subControls = QStyle::SC_SliderHandle | QStyle::SC_SliderTickmarks;

  style()->drawComplexControl(QStyle::CC_Slider, &opt, &p, this);
}

void QVolumeControl::setProgress(qreal value) {
  if (lastValue < value) {
    lastValue = value;
  } else if (lastValue > 0.0) {
    value = std::max(lastValue - 0.05, 0.0);
  } else {
    value = 0.0;
  }
  if (lastValue != value) {
    lastValue = value;
    update();
  }
}

void QVolumeControl::setFadeTimeout(quint32 timeout) {
  peakFadeTimer->stop();
  peakFadeTimer->start(timeout);
}
