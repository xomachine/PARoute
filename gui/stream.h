#ifndef OUTPUTSTREAM_H
#define OUTPUTSTREAM_H

#include "streaminfo.h"
#include <QFrame>
#include <QTimer>

namespace Ui {
class Stream;
}

class QGraphicsProxyWidget;
class SSBox;

class Stream : public QFrame {
  Q_OBJECT

public:
  explicit Stream(uint32_t idx, StreamInfo::Type, QWidget *parent = nullptr);
  ~Stream();

signals:
  void streamPeakChanged(qreal = 0.0);
  void moved(QPointF);
  void destinationChanged(quint32 index, quint32 target, bool is_input);
  void volumeChanged(quint32 stream, QVector<quint32> values, bool is_input);
  void detached(quint32 index, StreamInfo::Type);
  void muted(quint32 stream, bool muted, bool is_input);

public slots:
  void reinit(StreamInfo);
  void refreshPosition();
  void readPeak();

protected:
  virtual void moveEvent(QMoveEvent *event) override;
  virtual void mousePressEvent(QMouseEvent *) override;
  virtual void mouseReleaseEvent(QMouseEvent *) override;
  virtual void mouseMoveEvent(QMouseEvent *) override;

private slots:
  void changeVolume(int value);
  void mute(bool);
  void startIgnoringUpdates() { ignoreUpdates = true; }
  void quitIgnoringUpdates() { ignoreUpdates = false; }

private:
  void reflect();
  SSBox *getTarget(const QPointF &point);
  quint32 getTargetIndex(SSBox *) const;

  Ui::Stream *ui;
  uint32_t index;
  uint32_t connected_to;
  StreamInfo::Type type;
  QTimer wireRefresher;
  bool moving = false;
  QPointF dragStartMousePosition;
  bool ignoreUpdates = false;
  quint8 channels;
  SSBox *current_hover = nullptr;
  const volatile double *peak = nullptr;
};

#endif // OUTPUTSTREAM_H
