#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
  : QMainWindow(parent)
  , ui(new Ui::MainWindow) {
  ui->setupUi(this);
  connect(ui->actionShow_helper_modules, SIGNAL(toggled(bool)), this,
          SIGNAL(helperModuleVisibilityChanged(bool)));
}

MainWindow::~MainWindow() { delete ui; }

void MainWindow::setScene(QGraphicsScene *scene) {
  ui->moduleView->setScene(scene);
}

void MainWindow::logMessage(QString msg) {
  ui->logList->addItem(msg);
  ui->logList->scrollToBottom();
}

void MainWindow::on_actionQuit_triggered() { exit(0); }
