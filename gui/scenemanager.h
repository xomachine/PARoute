#ifndef SCENEMANAGER_H
#define SCENEMANAGER_H

#include "ssinfo.h"
#include "streaminfo.h"
#include <QGraphicsLinearLayout>
#include <QGraphicsScene>
#include <QGraphicsWidget>
#include <QObject>
#include <QTimer>

class SceneManager : public QObject {
  Q_OBJECT
public:
  explicit SceneManager(QObject *parent = nullptr);

  QGraphicsScene *getScene();

signals:
  void reinitModule(QString name, QString argument, QString props, uint32_t);
  void attachStream(QWidget *, StreamInfo);
  void helperModuleVisibilityChanged(bool);

public slots:
  void addModule(QString name, QString argument, QString props, int);
  void removeModule(int);
  void clear();
  void changeHelperModuleVisibility(bool visible);

private:
  QGraphicsScene scene;
  QGraphicsLinearLayout *layout;
  QGraphicsWidget *holder;
  QMap<int, QWidget *> modules;
  bool showHelperModules = false;
};

#endif // SCENEMANAGER_H
