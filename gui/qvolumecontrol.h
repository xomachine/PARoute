#ifndef QVOLUMECONTROL_H
#define QVOLUMECONTROL_H

#include <QProgressBar>
#include <QSlider>

class QVolumeControl : public QSlider {
  Q_OBJECT
public:
  QVolumeControl(QWidget *parent = nullptr);
  virtual void paintEvent(QPaintEvent *) override;

public slots:
  void setProgress(qreal value = 0.0);
  void setFadeTimeout(quint32 = 50);

private:
  QTimer *peakFadeTimer;
  qreal lastValue = 0.0;
};

#endif // QVOLUMECONTROL_H
