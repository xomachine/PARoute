#include "modulebox.h"
#include "rope.h"
#include "ssbox.h"
#include "stream.h"
#include "ui_modulebox.h"
#include <QGraphicsItem>
#include <QGraphicsProxyWidget>
#include <QGraphicsScene>

ModuleBox::ModuleBox(QString &name, QString &arg, QString &props, uint32_t idx,
                     QGraphicsItem *parent)
  : QFrame()
  , ui(new Ui::ModuleBox)
  , index(idx) {
  auto proxyWidget = new QGraphicsProxyWidget(parent);
  ui->setupUi(this);
  reinit(name, arg, props, idx);
  ui->snsContainer->hide();
  ui->inputs->hide();
  ui->outputs->hide();
  proxyWidget->setWidget(this);
  proxyWidget->setFlag(QGraphicsItem::ItemIsMovable);
}

ModuleBox::~ModuleBox() { delete ui; }

void ModuleBox::reinit(QString name, QString arg, QString props, uint32_t idx) {
  ui->moduleName->setText(name);
  setToolTip(props);
  if (arg.length() == 0) {
    ui->moduleArguments->hide();
  } else {
    ui->moduleArguments->setText(arg);
    ui->moduleArguments->setToolTip(arg);
  }
  index = idx;
  setVisible((!isEmpty()) || showWhenEmpty);
}

void ModuleBox::addSS(SSInfo source_or_sink) {
  if (source_or_sink.owner != index) {
    return;
  }
  auto ssindex = source_or_sink.type == SSInfo::Type::Source
                     ? source_or_sink.index
                     : source_or_sink.bound_index;
  auto exist = sources.find(ssindex);
  SSBox *result = nullptr;
  if (exist == sources.end()) {
    auto ss = new SSBox(ssindex, ui->snsContainer);
    connect(ss, SIGNAL(volumeChanged(quint32, QVector<quint32>, bool)),
            sender(), SLOT(changeSSVolume(quint32, QVector<quint32>, bool)));
    connect(ss, SIGNAL(muted(quint32, bool, bool)), sender(),
            SLOT(muteSS(quint32, bool, bool)));
    connect(ss, SIGNAL(defaultSSSet(QString, bool)), sender(),
            SLOT(setDefaultSS(QString, bool)));
    connect(sender(), SIGNAL(defaultSSChanged(QString, QString)), ss,
            SLOT(setDefaultSS(QString, QString)));
    connect(graphicsProxyWidget()->scene()->parent(),
            SIGNAL(attachStream(QWidget *, StreamInfo)), ss,
            SLOT(attachStream(QWidget *, StreamInfo)));
    ui->snsContainer->layout()->addWidget(ss);
    sources.insert(ssindex, ss);
    ui->snsContainer->show();
    graphicsProxyWidget()->createProxyForChildWidget(ss);
    result = ss;
  } else {
    result = exist.value();
  }
  setVisible(true);
  QMetaObject::invokeMethod(result, "reinit", Q_ARG(SSInfo, source_or_sink));
}

void ModuleBox::removeSS(int idx, SSInfo::Type type) {
  if (type != SSInfo::Type::Source) {
    return;
  }
  auto found = sources.find(idx);
  if (found == sources.end()) {
    return;
  }
  sources.erase(found);
  QMetaObject::invokeMethod(found.value(), "deleteLater");
  if (sources.size() == 0) {
    ui->snsContainer->hide();
  }
  setVisible((!isEmpty()) || showWhenEmpty);
}

static Stream *initStream(ModuleBox *self, const StreamInfo &info,
                          QObject *sender, QWidget *parent) {
  auto stream = new Stream(info.index, info.type, parent);

  QObject::connect(self, SIGNAL(reinitStream(StreamInfo)), stream,
                   SLOT(reinit(StreamInfo)));
  QObject::connect(
      stream, SIGNAL(volumeChanged(quint32, QVector<quint32>, bool)), sender,
      SLOT(changeStreamVolume(quint32, QVector<quint32>, bool)));
  QObject::connect(stream, SIGNAL(muted(quint32, bool, bool)), sender,
                   SLOT(muteStream(quint32, bool, bool)));
  return stream;
}

void ModuleBox::addStream(StreamInfo info) {
  if (info.owner != index) {
    return;
  }
  auto &container =
      info.type == StreamInfo::Output ? output_streams : input_streams;
  auto &uiContainer =
      info.type == StreamInfo::Output ? ui->outputs : ui->inputs;
  auto exist = container.find(info.index);
  if (exist == container.end()) {
    auto stream = initStream(this, info, sender(), ui->inputs);
    auto streamPlug = initStream(this, info, sender(), ui->inputs);
    connect(streamPlug, SIGNAL(destinationChanged(quint32, quint32, bool)),
            sender(), SLOT(changeStreamDestination(quint32, quint32, bool)));
    connect(streamPlug, SIGNAL(detached(quint32, StreamInfo::Type)), sender(),
            SIGNAL(removeStream(quint32, StreamInfo::Type)));
    connect(stream, SIGNAL(destroyed()), streamPlug, SLOT(deleteLater()));

    uiContainer->layout()->addWidget(stream);
    container.insert(info.index, stream);
    auto streamProxy = graphicsProxyWidget()->createProxyForChildWidget(stream);
    auto shift = 100.0;
    shift = info.type == StreamInfo::Output ? shift : -shift;
    auto rope = new Rope(shift, streamProxy);
    connect(stream, SIGNAL(moved(QPointF)), rope, SLOT(startMoved(QPointF)));
    connect(streamPlug, SIGNAL(moved(QPointF)), rope, SLOT(endMoved(QPointF)));
    emit streamOriginConnected(streamPlug, info);
  }
  uiContainer->show();
  setVisible(true);
  emit reinitStream(info);
}

void ModuleBox::removeStream(quint32 idx, StreamInfo::Type type) {
  auto &container = type == StreamInfo::Output ? output_streams : input_streams;
  auto exist = container.find(idx);
  if (exist == container.end()) {
    return;
  }
  if (output_streams.empty()) {
    ui->outputs->hide();
  }
  if (input_streams.empty()) {
    ui->inputs->hide();
  }
  QMetaObject::invokeMethod(exist.value(), "deleteLater");
  container.erase(exist);
  setVisible((!isEmpty()) || showWhenEmpty);
}

void ModuleBox::changeHelperModuleVisibility(bool visible) {
  showWhenEmpty = visible;
  setVisible((!isEmpty()) || showWhenEmpty);
}
