#ifndef MODULEBOX_H
#define MODULEBOX_H

#include "ssinfo.h"
#include "streaminfo.h"
#include <QFrame>

namespace Ui {
class ModuleBox;
}

class QGraphicsItem;
class SSBox;

class ModuleBox : public QFrame {
  Q_OBJECT
public:
  explicit ModuleBox(QString &name, QString &arg, QString &props, uint32_t idx,
                     QGraphicsItem *parent = nullptr);
  ~ModuleBox();

signals:
  void reinitStream(StreamInfo);
  void streamOriginConnected(QWidget *, StreamInfo);

public slots:
  void reinit(QString name, QString arg, QString props, uint32_t idx);
  void addSS(SSInfo source_or_sink);
  void removeSS(int idx, SSInfo::Type);
  void addStream(StreamInfo);
  void removeStream(quint32, StreamInfo::Type type);
  void changeHelperModuleVisibility(bool);

private:
  bool isEmpty() const {
    return sources.empty() && output_streams.empty() && input_streams.empty();
  }
  Ui::ModuleBox *ui;
  uint32_t index;
  QMap<uint32_t, SSBox *> sources;
  QMap<uint32_t, QWidget *> output_streams;
  QMap<uint32_t, QWidget *> input_streams;
  bool showWhenEmpty = false;
};

#endif // MODULEBOX_H
