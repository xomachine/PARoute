#include "rope.h"
#include <QPainter>
#include <QWidget>

Rope::Rope(qreal shift, QGraphicsItem *parent)
  : QGraphicsPathItem(parent)
  , shift(shift) {
  setAcceptHoverEvents(true);
}

void Rope::startMoved(QPointF pt) {
  start = mapFromScene(pt);
  // qWarning("start at %f x %f", pt.x(), pt.y());
  repaint();
}

void Rope::endMoved(QPointF pt) {
  end = mapFromScene(pt);
  // qWarning("end at %f x %f", pt.x(), pt.y());
  repaint();
}

void Rope::hoverEnterEvent(QGraphicsSceneHoverEvent *) { selected = true; }

void Rope::hoverLeaveEvent(QGraphicsSceneHoverEvent *) { selected = false; }

void Rope::repaint() {
  auto c1 = QPointF(start.x() + shift, start.y());
  auto c2 = QPointF(end.x() + shift, end.y());
  QPointF finish = shift > 0 ? end : start;
  QPainterPath arrow(finish);
  arrow.lineTo(finish.x() + shift * 0.1, finish.y() + 5.0);
  arrow.lineTo(finish.x() + shift * 0.1, finish.y() - 5.0);
  arrow.lineTo(finish);
  arrow.setFillRule(Qt::FillRule::WindingFill);
  QPainterPath path(start);
  path.cubicTo(c1, c2, end);
  path.cubicTo(c2, c1, start);
  // path.addEllipse(start, 20.0, 20.0);
  // path.addEllipse(end, 20.0, 20.0);
  path.addPath(arrow);
  QPen pen(Qt::PenStyle::SolidLine);
  pen.setWidthF(5.0);
  pen.setCosmetic(true);
  pen.setColor(QColor(selected ? "green" : "gray"));
  setPen(pen);
  setPath(path);
}
