#include "stream.h"
#include "ssbox.h"
#include "ui_stream.h"
#include <QGraphicsProxyWidget>
#include <QGraphicsScene>
#include <QMouseEvent>
#include <QMoveEvent>
#include <QStyle>

Stream::Stream(uint32_t idx, StreamInfo::Type t, QWidget *parent)
  : QFrame(parent)
  , ui(new Ui::Stream)
  , index(idx)
  , type(t) {
  ui->setupUi(this);
  if (t == StreamInfo::Input) {
    reflect();
  }
  ui->volumeControl->setRange(0, std::numeric_limits<uint16_t>::max());
  ui->muteButton->setIcon(style()->standardIcon(QStyle::SP_MediaVolumeMuted));
  connect(this, SIGNAL(streamPeakChanged(qreal)), ui->volumeControl,
          SLOT(setProgress(qreal)));
  connect(&wireRefresher, SIGNAL(timeout()), this, SLOT(refreshPosition()));
  connect(&wireRefresher, SIGNAL(timeout()), this, SLOT(readPeak()));
  connect(ui->volumeControl, SIGNAL(sliderPressed()), this,
          SLOT(startIgnoringUpdates()));
  connect(ui->volumeControl, SIGNAL(sliderReleased()), this,
          SLOT(quitIgnoringUpdates()));
  connect(ui->volumeControl, SIGNAL(sliderMoved(int)), this,
          SLOT(changeVolume(int)));
  connect(ui->muteButton, SIGNAL(toggled(bool)), this, SLOT(mute(bool)));
}

Stream::~Stream() { delete ui; }

void Stream::reinit(StreamInfo info) {
  if (info.index != index || info.type != type) {
    return;
  }
  qDebug("Reinit!");
  channels = info.channel_volumes.size();
  ui->volumeControl->setVisible(info.has_volume);
  ui->volumeControl->setDisabled(!info.volume_writable);
  ui->muteButton->setDown(info.mute);
  auto name = info.properties["application.name"];
  ui->streamBox->setTitle(name + ": " + info.name);
  ui->streamBox->setToolTip(info.serialized_properties);
  connected_to = info.source_or_sink;
  if (ignoreUpdates) {
    return;
  }
  ui->volumeControl->setValue(info.channel_volumes[0]);
  peak = info.peak;
  wireRefresher.start(100);
}

void Stream::refreshPosition() {
  if (!graphicsProxyWidget()) {
    return;
  }
  auto dest = graphicsProxyWidget()->scenePos();
  dest.setY(dest.y() + height() / 2);
  if (!ui->volumeControl->invertedAppearance()) {
    dest.setX(dest.x() + width());
  }
  emit moved(dest);
}

void Stream::readPeak() {
  if (!peak) {
    return;
  }
  const auto realpeak = *peak * (ui->volumeControl->value() /
                                 std::numeric_limits<uint16_t>::max());
  ui->volumeControl->setProgress(realpeak);
}

void Stream::moveEvent(QMoveEvent *event) {
  // qDebug("Move event");
  refreshPosition();
  QFrame::moveEvent(event);
}

void Stream::mousePressEvent(QMouseEvent *event) {
  dragStartMousePosition = event->localPos();
}

void Stream::mouseReleaseEvent(QMouseEvent *event) {
  moving = false;
  releaseMouse();
  if (current_hover) {
    current_hover->dropHover();
    current_hover = nullptr;
  }
  SSBox *target = getTarget(event->localPos());
  bool is_input = type == StreamInfo::Input;
  quint32 destination = quint32(-1);
  if (target && (destination = getTargetIndex(target)) != connected_to &&
      destination != quint32(-1)) {
    emit detached(index, type);
    emit destinationChanged(index, destination, is_input);
    return;
  }
  parentWidget()->layout()->invalidate();
  graphicsProxyWidget()->setParentItem(parentWidget()->graphicsProxyWidget());
}

void Stream::mouseMoveEvent(QMouseEvent *event) {
  if ((!moving) && parentWidget()->acceptDrops() /*if we are moving plug*/ &&
      event->buttons() & Qt::MouseButton::LeftButton &&
      (event->localPos() - dragStartMousePosition).manhattanLength() >=
          QApplication::startDragDistance()) {
    // Begin dragging
    moving = true;
    grabMouse();
    auto pos = graphicsProxyWidget()->scenePos();
    graphicsProxyWidget()->setParentItem(graphicsProxyWidget()->topLevelItem());
    graphicsProxyWidget()->setPos(pos);
  }

  if (moving) {
    SSBox *target = getTarget(event->localPos());
    if (current_hover != target) {
      if (current_hover) {
        current_hover->dropHover();
      }
      current_hover = target;
      if (target) {
        bool is_input = type == StreamInfo::Input;
        if (getTargetIndex(target) != connected_to) {
          QSize sz(size());
          target->dropHover(&sz, is_input);
        }
      }
    }
    if (!(event->buttons() & Qt::MouseButton::LeftButton)) {
      mouseReleaseEvent(event);
      return;
    }
    auto proxy = graphicsProxyWidget();
    auto newpos =
        proxy->mapToParent(event->localPos() - dragStartMousePosition);
    proxy->setPos(newpos);
  }
}

void Stream::changeVolume(int value) {
  QVector<quint32> volumes(channels);
  for (auto &volume : volumes) {
    volume = value;
  }
  emit volumeChanged(index, volumes, type == StreamInfo::Type::Input);
}

void Stream::mute(bool is_muted) {
  qDebug("Checked %d", is_muted);
  emit muted(index, is_muted, type == StreamInfo::Type::Input);
}

void Stream::reflect() {
  ui->horizontalLayout->setDirection(QBoxLayout::Direction::RightToLeft);
  ui->streamBox->setAlignment(Qt::AlignRight);
  ui->volumeControl->setInvertedControls(true);
  ui->volumeControl->setInvertedAppearance(true);
}

SSBox *Stream::getTarget(const QPointF &point) {
  auto proxy = graphicsProxyWidget();
  auto items = proxy->scene()->items(proxy->mapToScene(point));
  for (auto item : items) {
    auto realItem = dynamic_cast<QGraphicsProxyWidget *>(item);
    if (!realItem) {
      continue;
    }
    auto target = dynamic_cast<SSBox *>(realItem->widget());
    if (target) {
      return target;
    }
  }
  return nullptr;
}

quint32 Stream::getTargetIndex(SSBox *target) const {
  if (type == StreamInfo::Type::Input) {
    return target->getIndex();
  }
  return target->getSinkIndex();
}
