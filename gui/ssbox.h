#ifndef SSBOX_H
#define SSBOX_H

#include "ssinfo.h"
#include "streaminfo.h"
#include <QFrame>
#include <QTimer>

namespace Ui {
class SSBox;
}

class SSBox : public QFrame {
  Q_OBJECT

public:
  explicit SSBox(QWidget *parent = nullptr);
  SSBox(uint32_t index, QWidget *parent = nullptr);
  ~SSBox();

  quint32 getIndex() const { return index; }
  quint32 getSinkIndex() const { return sink_index; }
signals:
  void volumeChanged(quint32 index, QVector<quint32> volumes, bool is_sink);
  void muted(quint32 index, bool mute, bool is_sink);
  void defaultSSSet(QString index, bool is_sink);
  void normalizedPeakChanged(qreal);

public slots:
  void reinit(SSInfo source_or_sink);
  void setDefaultSS(QString name, QString sink_name);
  void readPeak();
  void attachStream(QWidget *, StreamInfo);
  void dropHover(QSize *dropSize = nullptr, bool is_input = false);

private slots:
  void changeVolumeSource(int value);
  void muteSource(bool is_muted);
  void changeVolumeSink(int value);
  void muteSink(bool is_muted);
  void setDefaultSink();
  void setDefaultSource();
  void startIgnoringUpdates() { ignoreUpdates = true; }
  void stopIgnoringUpdates() { ignoreUpdates = false; }

private:
  Ui::SSBox *ui;
  uint32_t index;
  uint32_t sink_index = -1;
  quint8 channels = 0;
  quint8 sink_channels = 0;
  QString name;
  QString sink_name;
  QTimer peaker;
  bool ignoreUpdates = false;
  const volatile double *sourcePeak = nullptr;
  const volatile double *sinkPeak = nullptr;
};

#endif // SSBOX_H
