#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QGraphicsScene>
#include <QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {
  Q_OBJECT

public:
  explicit MainWindow(QWidget *parent = nullptr);
  ~MainWindow();

  void setScene(QGraphicsScene *);

signals:
  void helperModuleVisibilityChanged(bool);
public slots:
  void logMessage(QString msg);

private slots:
  void on_actionQuit_triggered() __attribute__((noreturn));

private:
  Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
