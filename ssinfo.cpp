#include "ssinfo.h"
#include <pulse/introspect.h>
Q_DECLARE_METATYPE(SSInfo::Type);

static const auto mt = qRegisterMetaType<SSInfo>();
static const auto tmt = qRegisterMetaType<SSInfo::Type>();

template <class T> void initSSInfo(SSInfo &self, const T *info) {
  self.name = QString(info->name);
  self.index = info->index;
  self.description = QString(info->description);
  self.owner = info->owner_module;
  for (auto i = 0; i < info->volume.channels; ++i) {
    self.channel_volumes.append(info->volume.values[i]);
    self.normalized_volumes.append(
        double(info->volume.values[i]) /
        double(std::numeric_limits<uint16_t>::max()));
  }
  self.mute = info->mute;
  self.driver = QString(info->driver);
  void *state = nullptr;
  const char *propname = nullptr;
  while ((propname = pa_proplist_iterate(info->proplist, &state))) {
    const char *val = pa_proplist_gets(info->proplist, propname);
    if (val) {
      self.properties.insert(QString(propname), QString(val));
    }
  }
  self.serialized_properties = pa_proplist_to_string(info->proplist);
  self.volume_steps = info->n_volume_steps;
  self.card = info->card;
  for (uint32_t i = 0; i < info->n_ports; ++i) {
    auto port = info->ports[i];
    self.ports.append(PortInfo(port));
  }
  self.active_port = PortInfo(info->active_port);
}

SSInfo::SSInfo()
  : initialized(false) {}

SSInfo::SSInfo(const pa_sink_info *info) {
  initSSInfo(*this, info);
  bound_index = info->monitor_source;
  type = Type::Sink;
}

SSInfo::SSInfo(const pa_source_info *info) {
  initSSInfo(*this, info);
  bound_index = info->monitor_of_sink;
  type = Type::Source;
}

bool SSInfo::isEmpty() { return !initialized; }
