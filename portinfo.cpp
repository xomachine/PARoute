#include "portinfo.h"
#include <pulse/introspect.h>

static const auto pmt = qRegisterMetaType<PortInfo>();

template <class T> void initPortInfo(PortInfo &self, const T *info) {
  if (!info) {
    return;
  }
  self.name = info->name;
  self.description = info->description;
  self.priority = info->priority;
  self.available = info->available;
}

PortInfo::PortInfo(const pa_sink_port_info *info) { initPortInfo(*this, info); }

PortInfo::PortInfo(const pa_source_port_info *info) {
  initPortInfo(*this, info);
}
